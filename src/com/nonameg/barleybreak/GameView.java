package com.nonameg.barleybreak;


import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;


public class GameView extends SurfaceView {
	private SurfaceHolder holder;
	private Bitmap background;
	private Map<Integer, Sprite> sprites;
	private float bmpScale;
	private Board board;
	
    public GameView(Context context, Board board) {
        super(context);
        holder = getHolder();
        this.board = board;
        
        holder.addCallback(new SurfaceHolder.Callback() {
        	/*** ����������� ������� ��������� */
        	public void surfaceDestroyed(SurfaceHolder holder) {
        		
        	}

            /** �������� ������� ��������� */
        	public void surfaceCreated(SurfaceHolder holder) {
        		setBackground(R.drawable.small_field_background);
				createSprites(R.drawable.cutted_image);
				matchBoardAndSprites();
            }

            /** ��������� ������� ��������� */
        	public void surfaceChanged(SurfaceHolder holder, int format,
                             int width, int height) {

        	}
        });
        setWillNotDraw(false);

    }
    
	/**
     * ��������� ����
     * ��������� ����������
     */
    @Override
    protected void onDraw(Canvas canvas) 
    {
        canvas.drawColor(Color.BLACK);
        canvas.drawBitmap(background, 0, 0, null);  
        
        // ������ ����� �� ������ � �������� ��������� � ���������
    	for (Integer key : sprites.keySet()) {
    		Sprite sprite = sprites.get(key);
        	int scaledBorderShift = Math.round(Settings.borderShift / this.bmpScale);
        	sprite.onDraw(canvas, scaledBorderShift, scaledBorderShift);
    	}
    }
    

	/**
     * ������� ����� ������� �� �����
     * ���� �������� �� �����
     * �������� ��� ���������
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
	        case MotionEvent.ACTION_UP:
	            int currentX = (int)event.getX();
	            int currentY = (int)event.getY();
	            Sprite foundSprite = findSpriteInPoint(currentX, currentY);
	            if (foundSprite != null) {
	            	// ����� ������� � ��������� � �������� � ������� ����� ����
		            board.move(foundSprite.getRow(), foundSprite.getColumn(), true);
	            }
	           
        }

        return true;
    }
    
    @Override
    public void invalidate() {
    	matchBoardAndSprites();
    	super.invalidate();
    }
    
	/**
     * c������ ��������� �������� �� ��������� �������
     * @param resourse - �����������
     */
    private void createSprites(int resourse) {
    	sprites = new HashMap<Integer, Sprite>();
    	for (int idFrame = 0; idFrame < Settings.size * Settings.size; idFrame++) {
			Bitmap bmp = BitmapFactory.decodeResource(getResources(), resourse);
			int newWidth = Math.round(bmp.getWidth() / this.bmpScale);
			int newHeight = Math.round(bmp.getHeight() / this.bmpScale);
			Bitmap scaledBmp = Bitmap.createScaledBitmap(bmp, newWidth, newHeight, true);
			sprites.put(idFrame,new Sprite(scaledBmp, idFrame));
    	}    	
    }
    
	/**
     * ������������� ���
     * @param resourse - �����������
     */   
    private void setBackground(int resourse) {
		Bitmap bmp = BitmapFactory.decodeResource(getResources(), resourse);
		this.bmpScale = (float)bmp.getWidth()/(float)getWidth();
		int newWidth = Math.round(bmp.getWidth() / this.bmpScale);
		int newHeight = Math.round(bmp.getHeight() / this.bmpScale);
		background = Bitmap.createScaledBitmap(bmp, newWidth, newHeight, true);
    }
    
	/**
     * c������ ��������� �������� �� ��������� �������
     * @param x - ���������� ����� �� �
     * @param x - ���������� ����� �� y
     * @return ���������� ��� ��������, � ������� ��������� ������ �����
     */
    private Sprite findSpriteInPoint(int x, int y) {
    	for (Integer key : sprites.keySet()) {
    		Sprite sprite = sprites.get(key);
    		if (sprite.containsPoint(x, y))
    			return sprite;
    	}
    	return null;
    }

	/**
     * ������������ ����� � ��������� ��������
     */
    public void matchBoardAndSprites() {   	
    	int[][] field = board.getField();
        for (int i = 0; i < Settings.size; i++) {
            for (int j = 0; j < Settings.size; j++)
            {
            	Integer key = field[i][j]; 
            	Sprite sprite = sprites.get(key);
            	sprite.move(i, j);
            }
        }
        
    }

}
