package com.nonameg.barleybreak;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Board implements Observerable  {
	private int field[][];
	private List<GameObserver> observers;
	
	
    /**
     * on-demand ��������
     */
    public static class BorderHolder {
        public static final Board HOLDER_INSTANCE = new Board();
    }
    
    public static Board getInstance() {
        return BorderHolder.HOLDER_INSTANCE;
    }
	
	/**
     * ������� "����������" ���� 
    */
	private void generate(){
		field= new int[Settings.size][Settings.size];
		for (int i = 0; i < Settings.size * Settings.size; i++) {
			field[i / Settings.size][i % Settings.size] = i;
		}
	}
	
	/**
     * ������������ ����� �� ����
    */
	public void shuffle() {
		// ��������� ������������ ����
		generate();
		Random rand = new Random();
		// ������ ���������� ����������� ��������� ���
        for (int i = 0 ; i < 100; i++)
        {
            int x = rand.nextInt(100) % Settings.size;
            int y = rand.nextInt(100) % Settings.size;
            this.move(x, y, false);
        }
	}
	
	private Board() {
		observers = new ArrayList<GameObserver>();
		generate();
	}
	
	public int[][] getField() {
		return field;
	}
	
	public void setField(int[][] field) {
		this.field = field;
	}
	
	
	/**
     * �������� ����������� ����� ������ ����, ���� ��� ��������
     * @param row - ������ ������, ������� ���� �����������
     * @param col - ������� ������, ������� ���� �����������
     * @param gameOverChecking - ���������� �� ������ �������� ���� ��������� ��� ���
     */
	public void move(int row, int col, boolean gameOverChecking) {
        // ���������� ������ ������
        int emptyCol = -1, emptyRow = -1;
 
        // ���� ������ ������ �� ����
        for (int i = 0; i < Settings.size; i++) {
            for (int j = 0; j < Settings.size; j++) {
                if (this.field[i][j] == Settings.emptyPieceID) {
                	emptyCol = j;
                	emptyRow = i;
                }
            }
        }
 
        // ������� ��� ����
        // ������� ������ �������� - ������ ����� � ������ ���� ��� �������
        // �� �� �������� ����������� ������ ������
        if ((emptyCol == col || emptyRow == row) && (emptyCol != col || emptyRow != row)) {
        	// 
	        if (emptyCol == col) {
	            if (emptyRow < row) {
	                for (int i = emptyRow+1; i<= row; i++) {
	                    field[i-1][col] = field[i][col];
	                }
	            } else {
	                for (int i = emptyRow; i > row; i--) {
	                    field[i][col] = field[i-1][col];
	                }
	            }
	        }
	        if (emptyRow == row) {
	            if (emptyCol < col) {
	                for (int i = emptyCol+1; i <= col; i++) {
	                    field[row][i-1] = field[row][i];
	                }
	            } else {
	                for (int i = emptyCol; i > col; i--) {
	                    field[row][i] = field[row][i - 1];
	                }
	            }
	        }
	        field[row][col] = Settings.emptyPieceID;
	        if (gameOverChecking)
	        	gameOver();
	        notifyThatMoved();

        }
    }
	
	 
	/**
     * �������� ��������� ���� ��� ���
     * @return true - ���� ���������; false - ���
    */
	public boolean gameOver() { 
        for (int i = 0; i < Settings.size; i++) 
            for (int j = 0; j < Settings.size; j++) {
                if (this.field[i][j] != i * Settings.size + j) {
                	return false;
                }
            }
        // ���������� ����������, ��� ���� ���� ���������
        notifyThatGameOver();
        return true;
	}
	
	

	/**
     * ���������� ����������
     * @param observer - ���������
    */
	@Override
	public void addObserver(GameObserver observer) {
		observers.add(observer);	
	}

	@Override
	public void notifyThatGameOver() {
		for (GameObserver observer: observers) {
			observer.gameOver();
		}
		
	}
	
	@Override
	public void notifyThatMoved() {
		for (GameObserver observer: observers) {
			observer.boardChanged();
		}
		
	}

}
