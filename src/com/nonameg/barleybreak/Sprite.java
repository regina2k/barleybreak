package com.nonameg.barleybreak;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

public class Sprite {
	
	private Bitmap bmp;
	private int width, height;
	// ����� ������� �� �����
	private int column;
	// ����� ������ �� �����
	private int row;
	// ����� ������� �� �����������
	private int originImgColumn;
	// ����� ������ �� �����������
	private int originImgRow;
	private int x, y;
	
	public Sprite(Bitmap bmp, int frame) {
		this.bmp = bmp;
		this.width = bmp.getWidth() / Settings.size;
        this.height = bmp.getHeight() / Settings.size;
        this.column = frame % Settings.size;
        this.row = frame/ Settings.size;
		this.originImgColumn = column;
		this.originImgRow = row;
		
	}
	
	
	public void move(int row, int col) {
		this.column = col;
		this.row = row;
	}
	
	public int getColumn() {
		return this.column;
	}
	
	public int getRow() {
		return this.row;
	}
	
	/**
     * ������ ����: ��������� ���������� ��������� ��������, 
     * 	�������� �������� � ������ ��� � ������ ����� �� ������
     * @param canvas - �����
     * @param shiftX - �������� �� �
     * @param shiftY - �������� �� y
     */
	public void onDraw(Canvas canvas, int shiftX, int shiftY) {
		int srcX = this.originImgColumn * width;
        int srcY = this.originImgRow * height;
        Rect src = new Rect(srcX, srcY, srcX + width, srcY + height);
        x = shiftX + this.column * width;
        y = shiftY + this.row * height;
        Rect dst = new Rect(x, y, x + width, y + height);
		canvas.drawBitmap(bmp, src , dst, null);
	}
	
	/**
     * ��������� �� ������ ����� ������ �����
     * @param x - �������� ���������� x
     * @param y - �������� ���������� y
     * @return True - ���������, false - ���
     */
	public boolean containsPoint(int x, int y) {
		if (x >= this.x && x <= this.x + this.width && y >= this.y && y <= this.y + this.height)
			return true;
		return false;
	}
	

}
