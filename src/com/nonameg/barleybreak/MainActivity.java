package com.nonameg.barleybreak;


import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.Toast;


public class MainActivity extends Activity implements  GameObserver, SensorEventListener {

	private Board board;
	private static final int SHAKE_THRESHOLD = 800;
	private SensorManager sensorManager;
	private GameView gameView;
	private float x, y, z, last_x, last_y, last_z;
	private long lastUpdate;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// �������� �����
		board = Board.getInstance();
		// �������� ���������
		board.addObserver(this);
		gameView = new GameView(this, board);
		setContentView(gameView);
		
		// ���������� ��� ����, ����� ���������� ����������� �� ��������
		sensorManager=(SensorManager)getSystemService(SENSOR_SERVICE);
		sensorManager.registerListener(this, 
				sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
				SensorManager.SENSOR_DELAY_NORMAL);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public void gameOver() {
		Toast toast = Toast.makeText(getApplicationContext(), 
				"Game over! You win!",
				Toast.LENGTH_SHORT);
		toast.show();
		
	}
	
	@Override
	public void boardChanged() {
		gameView.invalidate();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
	    sensorManager.registerListener(this,
	        sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
	        SensorManager.SENSOR_DELAY_NORMAL);
	}
	
	@Override
	protected void onPause() {
	    super.onPause();
	    sensorManager.unregisterListener(this);
	}

	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onSensorChanged(SensorEvent event) {
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
			long curTime = System.currentTimeMillis();
		    if ((curTime - lastUpdate) > 100) {
		    	long diffTime = (curTime - lastUpdate);
		    	lastUpdate = curTime;

		    	x = event.values[0];
  				y = event.values[1];
  				z = event.values[2];

  				float speed = Math.abs(x + y + z - last_x - last_y - last_z) / diffTime * 10000;

  				if (speed > SHAKE_THRESHOLD) {
	  				board.shuffle();
	  				gameView.invalidate();
  				}
  				last_x = x;
  				last_y = y;
  				last_z = z;
		    }
		  }
		
	}

		
}
