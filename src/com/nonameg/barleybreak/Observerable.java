package com.nonameg.barleybreak;

public interface Observerable {
    void addObserver(GameObserver observer);
    void notifyThatGameOver();
    void notifyThatMoved();
}
