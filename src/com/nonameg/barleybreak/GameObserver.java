package com.nonameg.barleybreak;

/**
 * ��������� ��� ���������� �� �������
 */
public interface GameObserver {
	
	/**
	 * ���������� �� ��������� ����
	 */
	public void gameOver();
	
	/**
	 * ���������� �� ��������� ��������� �����
	 */
	public void boardChanged();
}
